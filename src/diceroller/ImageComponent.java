package diceroller;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.*;

import javax.imageio.ImageIO;

public class ImageComponent extends Component{
	BufferedImage img;

	public void paint(Graphics g) {
		super.paint(g);
        int w = getWidth();
        int h = getHeight();
        int imageWidth = img.getWidth(this);
        int imageHeight = img.getHeight(this);
        int x = (w - imageWidth)/2;
        int y = (h - imageHeight)/2;
        g.drawImage(img, x, y, this);
	}
	
	public ImageComponent(String path) {
		try {
			img = ImageIO.read(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Dimension getPreferredSize() {
		if (img==null) {
			return new Dimension(100,100);
		} else {
			return new Dimension(img.getWidth(), img.getHeight());
		}
	}
}
