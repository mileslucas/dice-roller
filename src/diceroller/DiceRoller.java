package diceroller;

import java.awt.*;
import java.awt.event.*;
import java.util.Random;
import javax.swing.*;
import javax.swing.border.*;

public class DiceRoller extends JFrame implements ActionListener {
	private static final Color BACKGROUND_COLOR = Color.white;
	private static final int NUM_COLUMNS = 6;
	private static final int BORDER_SIZE = 5;
	private static final int TEXTFIELD_WIDTH = 35;
	private static final Dimension GUI_SIZE = new Dimension(450, 185);
	private static final Dimension SPACER_SIZE = new Dimension(0, 5);

	// Text Fields
	private JTextField d20field;
	private JTextField d12field;
	private JTextField d10field;
	private JTextField d8field;
	private JTextField d6field;
	private JTextField d4field;
	private Random seed = new Random();

	public DiceRoller() {
		prepareGUI();
	}

	public static void main(String[] args) {
		DiceRoller diceRoller = new DiceRoller();
	}

	/**
	 * Prepares the GUI for class
	 */
	private void prepareGUI() {
		JPanel mainPanel = new JPanel(new GridLayout(0, NUM_COLUMNS));
		prepareMainPanel(mainPanel);
		setContentPane(mainPanel);
		setSize(GUI_SIZE);
		setTitle("D&D Dice Roller");
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				System.exit(0);
			}
		});
		setVisible(true);
	}

	/**
	 * Prepares the main panel
	 * 
	 * @param mainPanel
	 *            the main panel for the content frame
	 * 
	 */
	private void prepareMainPanel(JPanel mainPanel) {
		mainPanel.setBorder(new EmptyBorder(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE));
		JPanel d4Panel = new JPanel();
		d4Panel.setLayout(new BoxLayout(d4Panel, BoxLayout.Y_AXIS));
		prepareD4Panel(d4Panel);
		mainPanel.add(d4Panel);

		JPanel d6Panel = new JPanel();
		d6Panel.setLayout(new BoxLayout(d6Panel, BoxLayout.Y_AXIS));
		prepareD6Panel(d6Panel);
		mainPanel.add(d6Panel);

		JPanel d8Panel = new JPanel();
		d8Panel.setLayout(new BoxLayout(d8Panel, BoxLayout.Y_AXIS));
		prepareD8Panel(d8Panel);
		mainPanel.add(d8Panel);

		JPanel d10Panel = new JPanel();
		d10Panel.setLayout(new BoxLayout(d10Panel, BoxLayout.Y_AXIS));
		prepareD10Panel(d10Panel);
		mainPanel.add(d10Panel);

		JPanel d12Panel = new JPanel();
		d12Panel.setLayout(new BoxLayout(d12Panel, BoxLayout.Y_AXIS));
		prepareD12Panel(d12Panel);
		mainPanel.add(d12Panel);

		JPanel d20Panel = new JPanel();
		d20Panel.setLayout(new BoxLayout(d20Panel, BoxLayout.Y_AXIS));
		prepareD20Panel(d20Panel);
		mainPanel.add(d20Panel);

	}

	/**
	 * Prepares the panel with the functions for the d4 die
	 * 
	 * @param d4Panel
	 *            The panel that the d4 functions are being prepared for
	 */
	private void prepareD4Panel(JPanel d4Panel) {
		d4Panel.setBorder(new EmptyBorder(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE));
		d4Panel.setBackground(BACKGROUND_COLOR);
		JLabel label = new JLabel("D4");
		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		d4Panel.add(label);

		ImageComponent img = new ImageComponent("icons/d4.jpg");
		d4Panel.add(img);

		d4field = new JTextField();
		d4field.setEditable(false);
		d4field.setMaximumSize(new Dimension(TEXTFIELD_WIDTH, d4field.getPreferredSize().height));
		d4field.setAlignmentX(Component.CENTER_ALIGNMENT);
		d4field.setHorizontalAlignment(JTextField.CENTER);
		d4Panel.add(d4field);
		d4Panel.add(Box.createRigidArea(new Dimension(0, 5)));
		JButton button = new JButton("Roll");
		button.setAlignmentX(Component.CENTER_ALIGNMENT);
		button.addActionListener(this);
		button.setActionCommand("4");
		d4Panel.add(button);
	}

	/**
	 * Prepares the panel with the functions for the d6 die
	 * 
	 * @param d6Panel
	 *            The panel that the d6 functions are being prepared for
	 */
	private void prepareD6Panel(JPanel d6Panel) {
		d6Panel.setBorder(new EmptyBorder(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE));
		d6Panel.setBackground(BACKGROUND_COLOR);
		JLabel label = new JLabel("D6");
		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		d6Panel.add(label);

		ImageComponent img = new ImageComponent("icons/d6.jpg");
		d6Panel.add(img);

		d6field = new JTextField();
		d6field.setMaximumSize(new Dimension(TEXTFIELD_WIDTH, d6field.getPreferredSize().height));
		d6field.setEditable(false);
		d6field.setAlignmentX(Component.CENTER_ALIGNMENT);
		d6field.setHorizontalAlignment(JTextField.CENTER);
		d6Panel.add(d6field);
		d6Panel.add(Box.createRigidArea(SPACER_SIZE));
		JButton button = new JButton("Roll");
		button.setAlignmentX(Component.CENTER_ALIGNMENT);
		button.addActionListener(this);
		button.setActionCommand("6");
		d6Panel.add(button);

	}

	/**
	 * Prepares the panel with the functions for the d8 die
	 * 
	 * @param d8Panel
	 *            The panel that the d8 functions are being prepared for
	 */
	private void prepareD8Panel(JPanel d8Panel) {
		d8Panel.setBorder(new EmptyBorder(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE));
		d8Panel.setBackground(BACKGROUND_COLOR);
		JLabel label = new JLabel("D8");
		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		d8Panel.add(label);

		ImageComponent img = new ImageComponent("icons/d8.jpg");
		d8Panel.add(img);

		d8field = new JTextField();
		d8field.setMaximumSize(new Dimension(TEXTFIELD_WIDTH, d8field.getPreferredSize().height));
		d8field.setEditable(false);
		d8field.setAlignmentX(Component.CENTER_ALIGNMENT);
		d8field.setHorizontalAlignment(JTextField.CENTER);
		d8Panel.add(d8field);
		d8Panel.add(Box.createRigidArea(SPACER_SIZE));
		JButton button = new JButton("Roll");
		button.setAlignmentX(Component.CENTER_ALIGNMENT);
		button.addActionListener(this);
		button.setActionCommand("8");
		d8Panel.add(button);

	}

	/**
	 * Prepares the panel with the functions for the d10 die
	 * 
	 * @param d10Panel
	 *            The panel that the d10 functions are being prepared for
	 */
	private void prepareD10Panel(JPanel d10Panel) {
		d10Panel.setBorder(new EmptyBorder(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE));
		d10Panel.setBackground(BACKGROUND_COLOR);
		JLabel label = new JLabel("D10");
		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		d10Panel.add(label);

		ImageComponent img = new ImageComponent("icons/d10.jpg");
		d10Panel.add(img);

		d10field = new JTextField();
		d10field.setMaximumSize(new Dimension(TEXTFIELD_WIDTH, d10field.getPreferredSize().height));
		d10field.setEditable(false);
		d10field.setAlignmentX(Component.CENTER_ALIGNMENT);
		d10field.setHorizontalAlignment(JTextField.CENTER);
		d10Panel.add(d10field);
		d10Panel.add(Box.createRigidArea(SPACER_SIZE));
		JButton button = new JButton("Roll");
		button.setAlignmentX(Component.CENTER_ALIGNMENT);
		button.addActionListener(this);
		button.setActionCommand("10");
		d10Panel.add(button);

	}

	/**
	 * Prepares the panel with the functions for the d12 die
	 * 
	 * @param d12Panel
	 *            The panel that the d12 functions are being prepared for
	 */
	private void prepareD12Panel(JPanel d12Panel) {
		d12Panel.setBorder(new EmptyBorder(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE));
		d12Panel.setBackground(BACKGROUND_COLOR);
		JLabel label = new JLabel("D12");
		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		d12Panel.add(label);

		ImageComponent img = new ImageComponent("icons/d12.jpg");
		d12Panel.add(img);

		d12field = new JTextField();
		d12field.setMaximumSize(new Dimension(TEXTFIELD_WIDTH, d12field.getPreferredSize().height));
		d12field.setEditable(false);
		d12field.setAlignmentX(Component.CENTER_ALIGNMENT);
		d12field.setHorizontalAlignment(JTextField.CENTER);
		d12Panel.add(d12field);
		d12Panel.add(Box.createRigidArea(SPACER_SIZE));
		JButton button = new JButton("Roll");
		button.setAlignmentX(Component.CENTER_ALIGNMENT);
		button.addActionListener(this);
		button.setActionCommand("12");
		d12Panel.add(button);

	}

	/**
	 * Prepares the panel with the functions for the d20 die
	 * 
	 * @param d20Panel
	 *            The panel that the d20 functions are being prepared for
	 */
	private void prepareD20Panel(JPanel d20Panel) {
		d20Panel.setBorder(new EmptyBorder(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE));
		d20Panel.setBackground(BACKGROUND_COLOR);
		JLabel label = new JLabel("D20");
		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		d20Panel.add(label);

		ImageComponent img = new ImageComponent("icons/d20.jpg");
		d20Panel.add(img);

		d20field = new JTextField();
		d20field.setMaximumSize(new Dimension(TEXTFIELD_WIDTH, d20field.getPreferredSize().height));
		d20field.setEditable(false);
		d20field.setAlignmentX(Component.CENTER_ALIGNMENT);
		d20field.setHorizontalAlignment(JTextField.CENTER);
		d20Panel.add(d20field);
		d20Panel.add(Box.createRigidArea(SPACER_SIZE));
		JButton button = new JButton("Roll");
		button.setAlignmentX(Component.CENTER_ALIGNMENT);
		button.addActionListener(this);
		button.setActionCommand("20");
		d20Panel.add(button);

	}

	/**
	 * Simulates the roll of a die
	 * 
	 * @param seed
	 *            The generator for the random integer
	 * @param limit
	 *            The upper limit of the die
	 * @return A random integer between 1 and the given upper limit (inclusive)
	 */
	public String rollDie(int limit) {
		int roll = seed.nextInt(limit) + 1;
		return Integer.toString(roll);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		switch (Integer.parseInt(evt.getActionCommand())) {
		case 20:
			d20field.setText(rollDie(20));
			break;
		case 12:
			d12field.setText(rollDie(12));
			break;
		case 10:
			d10field.setText(rollDie(10));
			break;
		case 8:
			d8field.setText(rollDie(8));
			break;
		case 6:
			d6field.setText(rollDie(6));
			break;
		case 4:
			d4field.setText(rollDie(4));
			break;
		}
	}

}
